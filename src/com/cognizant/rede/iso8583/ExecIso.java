package com.cognizant.rede.iso8583;

import java.io.IOException;

import com.cognizant.rede.iso8583.controller.EcRequestController;
import com.cognizant.rede.iso8583.controller.PdvRequestController;
import com.cognizant.rede.iso8583.service.RetornoService;
import com.itko.lisa.vse.stateful.model.Request;
import com.itko.util.Parameter;
import com.itko.util.ParameterList;

@SuppressWarnings("deprecation")
public class ExecIso {

	String o = "select * from iso_erros where  \r\n" + "   ifnull( valor, \"700307\" ) = \"700307\" and\r\n"
			+ "   ifnull(exp, \"2101\") = \"2101\" and\r\n" + "   bandeira = \"Amex\";";

	public static void main(String[] args) throws IOException {

		//ValidaBitsService validaBits = new ValidaBitsService();
      
		Request request = new Request();
 
//		request.setOperation("0200");
//
//		ParameterList pl = new ParameterList();
//
//		pl.addParameter(new Parameter("2", "165448283487360007"));
//		pl.addParameter(new Parameter("3", "002000"));
//		pl.addParameter(new Parameter("4", "000000000100"));
//		pl.addParameter(new Parameter("7", "0208165615"));
//		pl.addParameter(new Parameter("11", "008561"));
//		pl.addParameter(new Parameter("14", "1901"));
//		pl.addParameter(new Parameter("18", "1337"));
//		pl.addParameter(new Parameter("42", "012341088      "));
//		pl.addParameter(new Parameter("43", "                       EDI           GBR"));
//		pl.addParameter(new Parameter("48", "019 420701032109203073"));
//		pl.addParameter(new Parameter("49", "986"));
//		pl.addParameter(new Parameter("124", "02600003800900019715285000000"));



		request.setOperation("0100");

		ParameterList pl = new ParameterList();

		pl.addParameter(new Parameter("2", "165448283487360007"));
		pl.addParameter(new Parameter("3", "003000"));
		pl.addParameter(new Parameter("4", "000000000100"));
		pl.addParameter(new Parameter("7", "0927122542"));
		pl.addParameter(new Parameter("11", "020050"));
		pl.addParameter(new Parameter("12", "092542"));
		pl.addParameter(new Parameter("13", "0927"));
		pl.addParameter(new Parameter("14", "2101"));
		pl.addParameter(new Parameter("22", "011"));
		pl.addParameter(new Parameter("32", "06400247"));
		pl.addParameter(new Parameter("41", "PV145729"));
		pl.addParameter(new Parameter("42", "145729016067304"));
		pl.addParameter(new Parameter("47", "308003021CL0503     P106a04   0060010015016C240F45CC33DCEF5016032EAB657F8D9B6A1A252AD163DA49E79D902002011111111111111112222026060SvrPrincVer====SvrRedeVer=====CliPrincVer====CliRedeVer=====027087PinPadManufacturer==PinPadModel========BibCompPPVer====BibRedeVer===KernelVer===SpecVer03000312305601429624338000104"));
		pl.addParameter(new Parameter("48", "025R1123S6              SS03"));
		pl.addParameter(new Parameter("49", "076"));
		pl.addParameter(new Parameter("61", "01100000040006"));
		pl.addParameter(new Parameter("123", "007L0503SE"));
		pl.addParameter(new Parameter("124", "020123456789012345     "));

		System.out.println(request);

		request.setArguments(pl);

		PdvRequestController rc = new PdvRequestController();
		rc.processaRequest(request, "resource/ISO8583-Rede.xml");

		System.out.println(rc.getRequest());

		for (RetornoService rt : rc.getLstretorno()) {
			System.out.println("Nome: " + rt.getKey());
			System.out.println("Valor: " + rt.getValue());
		}

	}

}
