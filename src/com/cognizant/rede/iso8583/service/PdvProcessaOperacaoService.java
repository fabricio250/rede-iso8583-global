package com.cognizant.rede.iso8583.service;

import com.cognizant.rede.devtest.RequestDevTest;
import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;


public class PdvProcessaOperacaoService {

    /**
     * @author Fabricio Moreira
     * @since 208-06-11
     */

    private static final int GENERATE_NSU = 2;
    private static final int GENERATE_AUTORIZATION_CODE = 6;
    private static final String GENERATE_TMP_BIT12 = "HHmmss";
    private static final String GENERATE_TMP_BIT13 = "MMdd";
    private RequestDevTest rd;
    private ArrayList<RetornoService> lstRetorno = new ArrayList<>();

    public PdvProcessaOperacaoService() {
    }

    public PdvProcessaOperacaoService(RequestDevTest rd) {
        this.rd = rd;
    }

    public void generateAuthorizationCode() {
        this.lstRetorno.add(new RetornoService("authorizationCode", generateCode(GENERATE_AUTORIZATION_CODE, true)));
    }

    public void generateNSUCode() {
        String nsu = String.valueOf(System.currentTimeMillis() / 1000) + generateCode(GENERATE_NSU, false);
        this.lstRetorno.add(new RetornoService("nsu", nsu));
    }

    private void validarParcela(String valor, String qtdParcelas) {

        Long vlr = null;
        int parc = 1;
        String parcelaValida = "false";
        qtdParcelas = (!StringUtils.isNotBlank(qtdParcelas)) ? "01" : qtdParcelas;

        if (Integer.parseInt(qtdParcelas.substring(0, 2)) == 1) {
            parcelaValida = "true";
        }

        if (StringUtils.isNotBlank(valor)) {
            vlr = Long.parseLong(valor);
            parc = Integer.parseInt(qtdParcelas.substring(0, 2));
        }

        if (vlr / parc >= 100) {
            parcelaValida = "true";
        }

        this.lstRetorno.add(new RetornoService("parcelaValida", parcelaValida));
    }

    public String generateCode(int quantidade, boolean alphanumeric) {
        Random random = new Random();

        String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String numeros = "0123456789";

        char[] alphabet = null;

        if (alphanumeric) {
            alphabet = (letras + numeros).toCharArray();
        } else {
            alphabet = numeros.toCharArray();
        }

        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= quantidade; i++) {
            int ch = random.nextInt(alphabet.length);
            sb.append(alphabet[ch]);
        }
        return sb.toString();
    }

    public void generateTimestamp(String str, String key) {
        Timestamp tmp = new Timestamp(System.currentTimeMillis());
        String strFinal = new SimpleDateFormat(str).format(tmp);
        this.lstRetorno.add(new RetornoService(key, strFinal));
    }

    public ArrayList<RetornoService> processar() {
        switch (rd.getOp()) {
            case "0100":
                this.getBit("2", "3", "4", "14");
                this.generateAuthorizationCode();
                this.generateNSUCode();
                this.generateTimestamp(GENERATE_TMP_BIT12, "tmpBit12");
                this.generateTimestamp(GENERATE_TMP_BIT13, "tmpBit13");
                this.validarParcela(rd.plGetParameterValue("4"), rd.plGetParameterValue("112"));
                break;
            case "0102":
                this.getBit("3", "37", "124");
                break;
            case "0120":
                this.getBit("3", "37");
                break;
            case "0200":
                this.getBit("2", "3", "4", "14");
                this.generateAuthorizationCode();
                this.generateNSUCode();
                this.generateTimestamp(GENERATE_TMP_BIT12, "tmpBit12");
                this.generateTimestamp(GENERATE_TMP_BIT13, "tmpBit13");
                this.validarParcela(rd.plGetParameterValue("4"), rd.plGetParameterValue("112"));
                break;
            case "0202":
                this.getBit("3", "37");
                break;
            case "0212":
                this.getBit("37");
                break;
            case "0220":
                this.getBit("3", "4", "37", "124");
                break;
            case "0230":
                this.getBit("37");
                this.generateAuthorizationCode();
                break;
            case "0400":
                this.getBit("3", "4", "37", "124");
                this.generateAuthorizationCode();
                break;
            case "0410":
                this.getBit("37");
                break;
        }

        return this.lstRetorno;
    }

    private void getBit(String... bits) {
        for (String bit : bits) {
            switch (bit) {
                case "2":
                    this.verificaBandeira(rd.plGetParameterValue(bit).trim());
                    this.lstRetorno.add(new RetornoService("numeroCartao", rd.plGetParameterValue(bit).trim()));
                    break;
                case "3":
                    switch (rd.plGetParameterValue(bit)) {
                        // 0 - Debito / 1 - Credito / 2 - IATA
                        case "002000":
                        case "200020":
                            this.lstRetorno.add(new RetornoService("tipoVenda", "0"));
                            break;
                        case "003000":
                        case "003800":
                            this.lstRetorno.add(new RetornoService("tipoVenda", "1"));
                            break;
                        case "200030":
                            this.lstRetorno.add(new RetornoService("tipoVenda", "2"));
                            break;
                        default:
                            break;
                    }
                    break;
                case "4":
                    this.lstRetorno.add(new RetornoService("valor", rd.plGetParameterValue(bit)));
                    break;
                case "11":
                    this.lstRetorno.add(new RetornoService("nsu", rd.plGetParameterValue(bit)));
                    break;
                case "14":
                    this.lstRetorno.add(new RetornoService("exp", rd.plGetParameterValue(bit)));
                    break;
                case "124":
                    this.lstRetorno.add(new RetornoService("tid", rd.plGetParameterValue(bit).substring(0, 20)));
                    break;
            }
        }
    }

    public void verificaBandeira(String numeroCartao) {

        if (numeroCartao != null && !"".equals(numeroCartao)) {
            switch (numeroCartao.substring(0, 1)) {
                case "5":
                    this.lstRetorno.add(new RetornoService("bandeira", "Master"));
                    break;
                case "4":
                    this.lstRetorno.add(new RetornoService("bandeira", "Visa"));
                    break;
                case "3":
                    if (numeroCartao.length() == 14) {
                        this.lstRetorno.add(new RetornoService("bandeira", "Dinners"));
                        break;
                    } else if (numeroCartao.length() == 15) {
                        this.lstRetorno.add(new RetornoService("bandeira", "Amex"));
                        break;
                    }
                case "6":
                    if (numeroCartao.substring(0, 2).equals("60")) {
                        this.lstRetorno.add(new RetornoService("bandeira", "Hipercard"));
                        break;
                    } else if (numeroCartao.substring(0, 2).equals("63")) {
                        this.lstRetorno.add(new RetornoService("bandeira", "Elo"));
                        break;
                    }
            }
        }

    }

}
