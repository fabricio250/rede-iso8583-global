package com.cognizant.rede.iso8583.service;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.cognizant.rede.devtest.RequestDevTest;
import com.itko.util.Parameter;

public class ValidaBitsService {
	
	/**
	 * @author Fabricio Moreira
	 * @since 208-06-11
	 */
	private ArrayList<String> arrayReq = new ArrayList<>();
	private ArrayList<String> arrayIso = new ArrayList<>();
	private String xml = "";
	private RequestDevTest devTest;
	
	public ValidaBitsService() {
	}
	
	public ValidaBitsService(RequestDevTest rd) {
		this.devTest = rd;
	}

	public void verificar(String caminho) throws IOException {
		extrairArgumentos();
		lerXml(caminho);
		extrairBits(devTest.getOp());
		conferirBits();
	}
	
	private void lerXml(String caminho) throws IOException {
		
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(new FileReader(caminho));
		while (br.ready()) {
			xml += br.readLine() + "\n";
		}
	}
	
	private void extrairBits(String op) {
		try {
			int ini = xml.indexOf("type=\""+op);
			int fim = xml.indexOf("</message>",ini);
			xml = xml.substring(ini,fim);
			String bitAtual = "";
			String bitAnterior = "0";
			while (xml.contains("bitnum")) {
				bitAtual = xml.substring(xml.indexOf("bitnum")+8,xml.indexOf("\" condition", xml.indexOf("bitnum")+8));
				if(Integer.valueOf(bitAtual)>Integer.valueOf(bitAnterior)) {
					arrayIso.add(bitAtual);
					bitAnterior = bitAtual;
				}
				xml = xml.substring(xml.indexOf("\" condition", xml.indexOf("bitnum")+8));
			}
		} catch (Exception e) {
			devTest.plAddParameter("OPERACAO_NAO_ENCONTRADA", op);
		}
	}
	
	private void conferirBits() {
		for (String string : arrayIso) {
			if(!arrayReq.contains(string)) {
				devTest.plAddParameter(string, "");
			}
		}
	}
	
	private void extrairArgumentos() {
		String arg = "";
		for (Parameter parameter : devTest.getPl()) {
			arg = parameter.getName().toString();
			arrayReq.add(arg);
		}
	}

	public RequestDevTest getDevTest() {
		return devTest;
	}

	public void setDevTest(RequestDevTest devTest) {
		this.devTest = devTest;
	}
	
	
}
