package com.cognizant.rede.iso8583.controller;

import java.io.IOException;
import java.util.ArrayList;


import com.cognizant.rede.devtest.RequestDevTest;
import com.cognizant.rede.iso8583.service.EcProcessaOperacaoService;
import com.cognizant.rede.iso8583.service.RetornoService;
import com.cognizant.rede.iso8583.service.ValidaBitsService;
import com.itko.lisa.vse.stateful.model.Request;

public class EcRequestController {
	
	/**
	 * @author Fabricio Moreira
	 * @since 208-06-11
	 */
	
	private Request request;
	private ArrayList<RetornoService> lstretorno;
	
	public EcRequestController processaRequest(Request request, String caminho) throws IOException {
		RequestDevTest rd = new RequestDevTest(request);
		ValidaBitsService vb = new ValidaBitsService(rd);
		EcProcessaOperacaoService po = new EcProcessaOperacaoService(rd);
		
		vb.verificar(caminho);
		
		lstretorno = po.processar();
		
		this.request = rd.getRequest();
		
		//this.request = vb.getDevTest().getRequest();
		
		
		return this;
	
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public ArrayList<RetornoService> getLstretorno() {
		return lstretorno;
	}

	public void setLstretorno(ArrayList<RetornoService> lstretorno) {
		this.lstretorno = lstretorno;
	}

}
